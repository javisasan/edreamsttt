## Tic Tac Toe as a Service

This repository is a Tic Tac Toe that will be used as a Service through an API in the future, but by the moment it will be called through command line.

Once you clone the repository, you need to perform a composer install:

**composer install**

in order to let composer generate the autoloading.

You can test the functionality by calling the following console command:

**php bin/console ttt:new-game**

After entering the names of the two players, a grid will appear, and the **command** script will ask you for positions.

Examples of valid positions are:

1a to 1c, 2a to 2c, 3a to 3c.

## Unit Tests

In order to launch the Unit Tests with PHPUnit (included through **composer install**) launch the command in the project root folder:

**./vendor/bin/phpunit**

Enjoy!