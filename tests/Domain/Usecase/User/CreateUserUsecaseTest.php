<?php

use PHPUnit\Framework\TestCase;
use App\Domain\Model\User;
use App\Domain\Repository\UserRepositoryInterface;
use App\Domain\Usecase\User\CreateUserUsecase;

class CreateUserUsecaseTest extends TestCase
{
    private $testUserName;

    /**
     * @before
     */
    public function setupFixture() {
        $this->testUserName = 'testUsername';
    }

    public function testExecute() {
        // Stub in order to avoid to store in DB
        $repositoryStub = $this->createMock(UserRepositoryInterface::class);
        $repositoryStub->method("save")
            ->willReturn(new User($this->testUserName));

        // Test the user creation
        $createUserUsecase = new CreateUserUsecase($repositoryStub);
        $newUser = $createUserUsecase->execute($this->testUserName);

        $this->assertEquals($this->testUserName, $newUser->getName());
    }

    public function testExecuteFailed() {
        // Stub in order to avoid to store in DB
        $repositoryStub = $this->createMock(UserRepositoryInterface::class);
        $repositoryStub->method("save")
            ->willReturn(new User('otherUser'));

        // Test the user creation
        $createUserUsecase = new CreateUserUsecase($repositoryStub);
        $newUser = $createUserUsecase->execute($this->testUserName);

        $this->assertNotEquals('foo', $newUser->getName());
    }
}
