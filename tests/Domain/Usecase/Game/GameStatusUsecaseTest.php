<?php

use PHPUnit\Framework\TestCase;
use App\Domain\Repository\GameRepositoryInterface;
use App\Domain\Repository\UserRepositoryInterface;
use App\Domain\Usecase\Game\AddMovementUsecase;
use App\Domain\Usecase\Game\CreateGameUsecase;
use App\Domain\Usecase\Game\GameStatusUsecase;
use App\Domain\Usecase\User\CreateUserUsecase;

class GameStatusUsecaseTest extends TestCase
{
    private $testUserNameA;
    private $testUserNameB;
    private $testUserA;
    private $testUserB;
    private $testGame;

    /**
     * @before
     */
    public function setupFixture() {
        // Prepare user names
        $this->testUserNameA = 'testUsernameA';
        $this->testUserNameB = 'testUsernameB';

        // Stub in order to avoid to store in DB
        $userRepositoryStub = $this->createMock(UserRepositoryInterface::class);
        $userRepositoryStub->method('save')
            ->willReturn('foo');

        //Create the mock users with name
        $createUserUsecase = new CreateUserUsecase($userRepositoryStub);
        $this->testUserA = $createUserUsecase->execute($this->testUserNameA);
        $this->testUserB = $createUserUsecase->execute($this->testUserNameB);

        // Stub for Game
        $gameRepositoryStub = $this->createMock(GameRepositoryInterface::class);
        $gameRepositoryStub->method('save')
            ->willReturn('foo');

        // Create the game
        $createGameUsecase = new CreateGameUsecase($gameRepositoryStub);
        $this->testGame = $createGameUsecase->execute($this->testUserA, $this->testUserB);
    }

    public function testExecuteWinsPlayerAVariant1() {
        $movementManager = new AddMovementUsecase($this->testGame);
        $statusManager   = new GameStatusUsecase($this->testGame);

        $this->assertTrue($movementManager->execute('1a'));
        $this->assertTrue($movementManager->execute('1b'));
        $this->assertTrue($movementManager->execute('2a'));
        $this->assertTrue($movementManager->execute('2b'));

        $gameStatus = $statusManager->execute();
        $this->assertFalse($gameStatus['finished']);
        $this->assertEmpty($gameStatus['winner']);

        $this->assertTrue($movementManager->execute('3a'));

        $gameStatus = $statusManager->execute();
        $this->assertTrue($gameStatus['finished']);
        $this->assertNotEmpty($gameStatus['winner']);
        $this->assertEquals($this->testUserA, $gameStatus['winner']);
    }

    public function testExecuteWinsPlayerAVariant2() {
        $movementManager = new AddMovementUsecase($this->testGame);
        $statusManager   = new GameStatusUsecase($this->testGame);

        $this->assertTrue($movementManager->execute('1b'));
        $this->assertTrue($movementManager->execute('1c'));
        $this->assertTrue($movementManager->execute('2b'));
        $this->assertTrue($movementManager->execute('1a'));

        $gameStatus = $statusManager->execute();
        $this->assertFalse($gameStatus['finished']);
        $this->assertEmpty($gameStatus['winner']);

        $this->assertTrue($movementManager->execute('3b'));

        $gameStatus = $statusManager->execute();
        $this->assertTrue($gameStatus['finished']);
        $this->assertNotEmpty($gameStatus['winner']);
        $this->assertEquals($this->testUserA, $gameStatus['winner']);
    }

    public function testExecuteWinsPlayerAVariant3() {
        $movementManager = new AddMovementUsecase($this->testGame);
        $statusManager   = new GameStatusUsecase($this->testGame);

        $this->assertTrue($movementManager->execute('1c'));
        $this->assertTrue($movementManager->execute('1a'));
        $this->assertTrue($movementManager->execute('2c'));
        $this->assertTrue($movementManager->execute('1b'));

        $gameStatus = $statusManager->execute();
        $this->assertFalse($gameStatus['finished']);
        $this->assertEmpty($gameStatus['winner']);

        $this->assertTrue($movementManager->execute('3c'));

        $gameStatus = $statusManager->execute();
        $this->assertTrue($gameStatus['finished']);
        $this->assertNotEmpty($gameStatus['winner']);
        $this->assertEquals($this->testUserA, $gameStatus['winner']);
    }

    public function testExecuteWinsPlayerAVariant4() {
        $movementManager = new AddMovementUsecase($this->testGame);
        $statusManager   = new GameStatusUsecase($this->testGame);

        $this->assertTrue($movementManager->execute('1a'));
        $this->assertTrue($movementManager->execute('2b'));
        $this->assertTrue($movementManager->execute('1b'));
        $this->assertTrue($movementManager->execute('3c'));

        $gameStatus = $statusManager->execute();
        $this->assertFalse($gameStatus['finished']);
        $this->assertEmpty($gameStatus['winner']);

        $this->assertTrue($movementManager->execute('1c'));

        $gameStatus = $statusManager->execute();
        $this->assertTrue($gameStatus['finished']);
        $this->assertNotEmpty($gameStatus['winner']);
        $this->assertEquals($this->testUserA, $gameStatus['winner']);
    }

    public function testExecuteWinsPlayerAVariant5() {
        $movementManager = new AddMovementUsecase($this->testGame);
        $statusManager   = new GameStatusUsecase($this->testGame);

        $this->assertTrue($movementManager->execute('1b'));
        $this->assertTrue($movementManager->execute('2a'));
        $this->assertTrue($movementManager->execute('2b'));
        $this->assertTrue($movementManager->execute('3c'));

        $gameStatus = $statusManager->execute();
        $this->assertFalse($gameStatus['finished']);
        $this->assertEmpty($gameStatus['winner']);

        $this->assertTrue($movementManager->execute('3b'));

        $gameStatus = $statusManager->execute();
        $this->assertTrue($gameStatus['finished']);
        $this->assertNotEmpty($gameStatus['winner']);
        $this->assertEquals($this->testUserA, $gameStatus['winner']);
    }

    public function testExecuteWinsPlayerAVariant6() {
        $movementManager = new AddMovementUsecase($this->testGame);
        $statusManager   = new GameStatusUsecase($this->testGame);

        $this->assertTrue($movementManager->execute('1c'));
        $this->assertTrue($movementManager->execute('2a'));
        $this->assertTrue($movementManager->execute('2c'));
        $this->assertTrue($movementManager->execute('3b'));

        $gameStatus = $statusManager->execute();
        $this->assertFalse($gameStatus['finished']);
        $this->assertEmpty($gameStatus['winner']);

        $this->assertTrue($movementManager->execute('3c'));

        $gameStatus = $statusManager->execute();
        $this->assertTrue($gameStatus['finished']);
        $this->assertNotEmpty($gameStatus['winner']);
        $this->assertEquals($this->testUserA, $gameStatus['winner']);
    }

    public function testExecuteWinsPlayerAVariant7() {
        $movementManager = new AddMovementUsecase($this->testGame);
        $statusManager   = new GameStatusUsecase($this->testGame);

        $this->assertTrue($movementManager->execute('1a'));
        $this->assertTrue($movementManager->execute('2a'));
        $this->assertTrue($movementManager->execute('2b'));
        $this->assertTrue($movementManager->execute('3b'));

        $gameStatus = $statusManager->execute();
        $this->assertFalse($gameStatus['finished']);
        $this->assertEmpty($gameStatus['winner']);

        $this->assertTrue($movementManager->execute('3c'));

        $gameStatus = $statusManager->execute();
        $this->assertTrue($gameStatus['finished']);
        $this->assertNotEmpty($gameStatus['winner']);
        $this->assertEquals($this->testUserA, $gameStatus['winner']);
    }

    public function testExecuteWinsPlayerBVariant8() {
        $movementManager = new AddMovementUsecase($this->testGame);
        $statusManager   = new GameStatusUsecase($this->testGame);

        $this->assertTrue($movementManager->execute('1a'));
        $this->assertTrue($movementManager->execute('3a'));
        $this->assertTrue($movementManager->execute('1b'));
        $this->assertTrue($movementManager->execute('2b'));
        $this->assertTrue($movementManager->execute('2c'));

        $gameStatus = $statusManager->execute();
        $this->assertFalse($gameStatus['finished']);
        $this->assertEmpty($gameStatus['winner']);

        $this->assertTrue($movementManager->execute('1c'));

        $gameStatus = $statusManager->execute();
        $this->assertTrue($gameStatus['finished']);
        $this->assertNotEmpty($gameStatus['winner']);
        $this->assertEquals($this->testUserB, $gameStatus['winner']);
    }

    public function testExecuteDeuce() {
        $movementManager = new AddMovementUsecase($this->testGame);
        $statusManager   = new GameStatusUsecase($this->testGame);

        $this->assertTrue($movementManager->execute('1a'));
        $this->assertTrue($movementManager->execute('2b'));
        $this->assertTrue($movementManager->execute('2a'));
        $this->assertTrue($movementManager->execute('3a'));
        $this->assertTrue($movementManager->execute('1c'));
        $this->assertTrue($movementManager->execute('1b'));
        $this->assertTrue($movementManager->execute('3b'));
        $this->assertTrue($movementManager->execute('2c'));

        $gameStatus = $statusManager->execute();
        $this->assertFalse($gameStatus['finished']);
        $this->assertEmpty($gameStatus['winner']);

        $this->assertTrue($movementManager->execute('3c'));

        $gameStatus = $statusManager->execute();
        $this->assertTrue($gameStatus['finished']);
        $this->assertEmpty($gameStatus['winner']);
    }

}
