<?php

use PHPUnit\Framework\TestCase;
use App\Domain\Exception\CellNotEmptyException;
use App\Domain\Exception\IllegalMoveException;
use App\Domain\Exception\InvalidPositionException;
use App\Domain\Repository\GameRepositoryInterface;
use App\Domain\Repository\UserRepositoryInterface;
use App\Domain\Usecase\Game\AddMovementUsecase;
use App\Domain\Usecase\Game\CreateGameUsecase;
use App\Domain\Usecase\User\CreateUserUsecase;

class AddMovementUsecaseTest extends TestCase
{
    private $testUserNameA;
    private $testUserNameB;
    private $testUserA;
    private $testUserB;
    private $testGame;

    /**
     * @before
     */
    public function setupFixture() {
        // Prepare user names
        $this->testUserNameA = 'testUsernameA';
        $this->testUserNameB = 'testUsernameB';

        // Stub in order to avoid to store in DB
        $userRepositoryStub = $this->createMock(UserRepositoryInterface::class);
        $userRepositoryStub->method('save')
            ->willReturn('foo');

        //Create the mock users with name
        $createUserUsecase = new CreateUserUsecase($userRepositoryStub);
        $this->testUserA = $createUserUsecase->execute($this->testUserNameA);
        $this->testUserB = $createUserUsecase->execute($this->testUserNameB);

        // Stub for Game
        $gameRepositoryStub = $this->createMock(GameRepositoryInterface::class);
        $gameRepositoryStub->method('save')
            ->willReturn('foo');

        // Create the game
        $createGameUsecase = new CreateGameUsecase($gameRepositoryStub);
        $this->testGame = $createGameUsecase->execute($this->testUserA, $this->testUserB);
    }

    public function testExecute() {
        $movementManager = new AddMovementUsecase($this->testGame);

        $this->assertTrue($movementManager->execute('1a'));
        $this->assertTrue($movementManager->execute('1b'));
    }

    public function testExecuteInvalidPosition() {
        $movementManager = new AddMovementUsecase($this->testGame);

        try {
            $movementManager->execute('5f');
            $this->assertTrue(false);
        } catch (\Exception $e) {
            $this->assertTrue($e instanceof InvalidPositionException);
        }
    }

    public function testExecuteCellAlreadyInUse() {
        $movementManager = new AddMovementUsecase($this->testGame);

        try {
            $movementManager->execute('1a');
            $movementManager->execute('1a');
            $this->assertTrue(false);
        } catch (\Exception $e) {
            $this->assertTrue($e instanceof CellNotEmptyException);
        }
    }

    public function testExecuteGridAlreadyFull() {
        $movementManager = new AddMovementUsecase($this->testGame);

        try {
            $movementManager->execute('1a');
            $movementManager->execute('1b');
            $movementManager->execute('1c');
            $movementManager->execute('2a');
            $movementManager->execute('2b');
            $movementManager->execute('2c');
            $movementManager->execute('3a');
            $movementManager->execute('3b');
            $movementManager->execute('3c');
            $movementManager->execute('1a');
            $this->assertTrue(false);
        } catch (\Exception $e) {
            $this->assertTrue($e instanceof IllegalMoveException);
        }
    }
}
