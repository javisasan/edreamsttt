<?php

use PHPUnit\Framework\TestCase;
use App\Domain\Model\Game;
use App\Domain\Model\Board;
use App\Domain\Repository\GameRepositoryInterface;
use App\Domain\Repository\UserRepositoryInterface;
use App\Domain\Usecase\Game\CreateGameUsecase;
use App\Domain\Usecase\User\CreateUserUsecase;

class CreateGameUsecaseTest extends TestCase
{
    private $testUserNameA;
    private $testUserNameB;
    private $testUserA;
    private $testUserB;
    private $gameBoardMock;

    /**
     * @before
     */
    public function setupFixture() {
        // Prepare user names
        $this->testUserNameA = 'testUsernameA';
        $this->testUserNameB = 'testUsernameB';

        // Stub in order to avoid to store in DB
        $repositoryStub = $this->createMock(UserRepositoryInterface::class);
        $repositoryStub->method("save")
            ->willReturn('foo');

        //Create the mock users with name
        $createUserUsecase = new CreateUserUsecase($repositoryStub);
        $this->testUserA = $createUserUsecase->execute($this->testUserNameA);
        $this->testUserB = $createUserUsecase->execute($this->testUserNameB);

        // Create board mock
        $this->gameBoardMock = $this->createMock(Board::class);
    }

    public function testExecute() {
        // Stub in order to avoid to store in DB
        $repositoryStub = $this->createMock(GameRepositoryInterface::class);
        $repositoryStub->method("save")
            ->willReturn(new Game($this->testUserA, $this->testUserB, $this->gameBoardMock));

        // Test the game creation
        $createGameUsecase = new CreateGameUsecase($repositoryStub);
        $newGame = $createGameUsecase->execute($this->testUserA, $this->testUserB, $this->gameBoardMock);

        $this->assertEquals($this->testUserA, $newGame->getUserA());
        $this->assertEquals($this->testUserB, $newGame->getUserB());

        $this->assertNotEquals($this->testUserA, $newGame->getUserB());
        $this->assertNotEquals($this->testUserB, $newGame->getUserA());
    }
}
