<?php

namespace App\Infrastructure\Repository\Game;

use App\Domain\Model\Game;
use App\Domain\Repository\GameRepositoryInterface;

class GameMongoRepository implements GameRepositoryInterface
{
    public function save(Game $game) {
        echo "*** Game between " . $game->getUserA()->getName() . " and " . $game->getUserB()->getName(). " should be stored in MongoDB!\n";
    }
}