<?php

namespace App\Domain\Usecase\User;

use App\Domain\Model\User;
use App\Domain\Repository\UserRepositoryInterface;

class CreateUserUsecase
{
    private $userRepositoryInterface;

    public function __construct(UserRepositoryInterface $userRepositoryInterface) {
        $this->userRepositoryInterface = $userRepositoryInterface;
    }

    public function execute($userName) {
        $user = new User($userName);

        $this->userRepositoryInterface->save($user);

        return $user;
    }
}