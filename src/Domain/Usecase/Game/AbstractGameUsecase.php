<?php

namespace App\Domain\Usecase\Game;

abstract class AbstractGameUsecase
{
    protected $game;

    public function __construct($game)
    {
        $this->game = $game;
    }
}