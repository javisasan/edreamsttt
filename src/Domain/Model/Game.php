<?php

namespace App\Domain\Model;

use App\Domain\Model\User;
use App\Domain\Model\Board;

class Game
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Board
     */
    private $board;

    /**
     * @var User
     */
    private $userA;

    /**
     * @var User
     */
    private $userB;

    /**
     * @var User
     */
    private $currentUser;

    /**
     * @var User
     */
    private $nextUser;

    /**
     * @var int
     */
    private $movements;

    /**
     * @var bool
     */
    private $gameFinished;

    /**
     * @var User
     */
    private $winner;

    public function __construct(User $userA, User $userB, Board $board)
    {
        $this->userA        = $userA;
        $this->userB        = $userB;
        $this->board        = $board;
        $this->movements    = 0;
        $this->currentUser  = null;
        $this->nextUser     = $this->userA;
        $this->gameFinished = false;
        $this->winner       = null;
    }

    /**
     * @return \App\Domain\Model\User
     */
    public function getUserA() :User
    {
        return $this->userA;
    }

    /**
     * @return \App\Domain\Model\User
     */
    public function getUserB() :User
    {
        return $this->userB;
    }

    /**
     * @return \App\Domain\Model\Board
     */
    public function getBoard() :Board
    {
        return $this->board;
    }

    /**
     * Adds a movement: upgrades the movement count and sets the following movement user
     */
    public function addMovement() :void
    {
        // Increase number of movements
        $this->movements ++;

        $this->currentUser = $this->nextUser;

        if ($this->nextUser == $this->userA) {
            $this->nextUser = $this->userB;
        } else {
            $this->nextUser = $this->userA;
        }
    }

    /**
     * Gets the movement count
     *
     * @return int
     */
    public function getMovements() :int
    {
        return $this->movements;
    }

    /**
     * Returns if the game has finished or not
     *
     * @return bool
     */
    public function isFinished() :bool
    {
        return $this->gameFinished;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * This method finishes the game
     */
    public function setGameFinished(): void
    {
        $this->gameFinished = true;
    }

    /**
     * Current turn user getter
     *
     * @return \App\Domain\Model\User
     */
    public function getCurrentUser(): User
    {
        return $this->currentUser;
    }

    /**
     * Next turn user getter
     *
     * @return \App\Domain\Model\User
     */
    public function getNextUser(): User
    {
        return $this->nextUser;
    }

    /**
     * Sets the winner of the game
     *
     * @param \App\Domain\Model\User $user
     */
    public function setWinner(User $user): void
    {
        $this->winner = $user;
    }

    /**
     * Gets the winner of the game
     *
     * @return \App\Domain\Model\User|null
     */
    public function getWinner(): ?User
    {
        return $this->winner;
    }

}