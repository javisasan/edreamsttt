<?php

namespace App\Domain\Exception;

class IllegalMoveException extends \RuntimeException
{
    protected $message;

    public function __construct($message) {
        $this->message = $message;
    }
}