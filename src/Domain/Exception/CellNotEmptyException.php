<?php

namespace App\Domain\Exception;

class CellNotEmptyException extends IllegalMoveException
{
}